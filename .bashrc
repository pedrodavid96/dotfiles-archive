#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

include () {
	if [[ -f "$1" ]]; then source "$1";
	else echo "[Warning] File \"$1\" does not exist";
	fi
}

export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CACHE_HOME=$HOME/.cache

include $XDG_CONFIG_HOME/bash/xdg_workarounds
include $XDG_CONFIG_HOME/bash/bash_aliases

# If on a graphical environment change terminal prompt to be colored and display git info
if [[ $DISPLAY ]]; then
	include $XDG_CONFIG_HOME/bash/bash_git
else
	PS1='[\u@\h \W]\$ '
fi

# Add opt/bin (symlinks to opt/<program>/<program.sh>)
PATH=$PATH:$HOME/opt/bin

export EDITOR=vim

# If not on a graphical environment ($DISPLAY is null or empty) and first tty start graphical environment
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
	exec startx "$XDG_CONFIG_HOME/X11/xinitrc"
fi
